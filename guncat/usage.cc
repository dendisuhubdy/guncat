//                     usage.cc

#include "main.ih"

namespace
{
    char const info[] = R"R([options] [file(s)]
   Where:
   [options] - optional arguments (short options between parentheses):

     --errors-OK: processing continues even if gpg returns a non-zero
                  exit value
     --gpg <path>:  path to the gpg program (default: /usr/bin/gpg)
     --gpg-msg (-m) path: specify - to write gpg's messages to stderr,
                  otherwise write messages to `path' (by default messages
                  are suppressed)
      --gpg-option "spec": gpg option `spec' is passed to gpg child processes
      --help (-h): provide this help
      --passphrase (-p): the passphrase is read as the first line from 
                  stdin, (without being echoed); otherwise the passphrase 
                  is handled by gpg itself (e.g., using gpg-agent)
      --show-gpg: show the gpg command that would be used, and quit.
      --time-limit (-T) <seconds>: maximum allowed time in seconds for
                  decrypting an encrypted section (by default: no time limit
                  is used)
      --tty-OK (-t): by default gpg's option --no-tty is used.
      --verbose [0-2]: gpg's verbosity level (by default: gpg's --quiet
                  option is used)
      --version (-v)   - show version information and terminate

   [file(s)]   - file(s) to process. If not specified or if specified as
                 - (also within a sequence of file names) the standard input
                 stream is read.
                 The processed information is written to the standard output
                 stream.

)R";

}

void usage(std::string const &progname)
{
    cout << "\n" <<
    progname << " by " << Icmbuild::author << "\n" <<
    progname << " V" << Icmbuild::version << " " << Icmbuild::years << "\n"
    "\n"
    "Usage: " << progname << info;
}

