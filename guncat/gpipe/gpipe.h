#ifndef INCLUDED_GPIPE_
#define INCLUDED_GPIPE_

#include <bobcat/pipe>

class GPipe: public FBB::Pipe
{
    public:
        ~GPipe();               // closes the pipes
};
        
#endif
