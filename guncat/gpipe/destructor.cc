#include "gpipe.ih"

GPipe::~GPipe()
{
    close(readFd());
    close(writeFd());
}
