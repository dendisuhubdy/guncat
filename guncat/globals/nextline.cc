#include "globals.ih"

istream &nextline(istream &in, string &line)
{
    if (getline(in, line))
        ++g_lineNr;

    return in;
}
