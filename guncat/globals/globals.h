#ifndef INCLUDED_GLOBALS_H_
#define INCLUDED_GLOBALS_H_

#include <iosfwd>

extern size_t g_lineNr;
extern std::string g_filename;

std::string fileInfo();
std::istream &nextline(std::istream &in, std::string &line);

#endif
