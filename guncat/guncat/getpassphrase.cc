#include "guncat.ih"

void Guncat::getPassphrase(istream &in)
{
    if (not nextline(in, d_passphrase))
        throw Exception{} << fileInfo() << ": no line";

    d_getPassphrase = &Guncat::nop;
    ++g_lineNr;
}
