#ifndef INCLUDED_GUNCAT_
#define INCLUDED_GUNCAT_

#include <iosfwd>
#include <string>

#include "../decryptor/decryptor.h"

namespace FBB
{
    class Arg;
}

class Guncat
{
    enum CinSpec
    {
        FIRST,
        DONE,
        WARNED
    };

    CinSpec d_processedCin = FIRST;
    
    FBB::Arg &d_arg;
    Decryptor d_decryptor;
                                            // sets the passphrase at -p
                                            // from the 1st line of the 1st
                                            // input stream
    void (Guncat::*d_getPassphrase)(std::istream &in);
    std::string d_passphrase;

    public:
        Guncat();
        void arguments();

    private:
        void process(std::istream &in);
        void processCin();
        void processFileArguments();

        void getPassphrase(std::istream &in);
        void nop(std::istream &in);         // no pwd retrieval: no-op.

};
        
#endif


