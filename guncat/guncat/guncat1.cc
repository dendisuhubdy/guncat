#include "guncat.ih"

Guncat::Guncat()
:
    d_arg(Arg::instance()),
    d_getPassphrase(d_arg.option('p') ? &Guncat::getPassphrase : &Guncat::nop)
{
    wmsg.reset(cerr);               // warnings to cerr

    if (d_arg.option(0, "show-gpg"))
        d_decryptor.showGPG();      // ends the program with return value 0.
}                               
