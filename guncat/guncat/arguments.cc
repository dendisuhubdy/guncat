#include "guncat.ih"

// `Guncat' all files specified as the program's arguments in turn.
// if no arguments are provided, process stdin
// The constructor has initialized a Decryptor object, which has read the
// first stdin line as the passphrase.

void Guncat::arguments()
{
    d_decryptor.setPassphrase(d_passphrase);

    if (d_arg.nArgs() != 0)
        processFileArguments();
    else
    {
        g_filename = "stdin";
        process(cin);
    }
}
