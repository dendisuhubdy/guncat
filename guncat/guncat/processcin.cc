#include "guncat.ih"

void Guncat::processCin()
{
    g_filename = "stdin";
    switch (d_processedCin)
    {
        case FIRST:
            process(cin);
            d_processedCin = DONE;
        break;

        case DONE:
            wmsg << "Multiple stdin (`-') specifications are ignored" << endl;
            d_processedCin = WARNED;
        break;
    
        case WARNED:
        break;
    }
}





