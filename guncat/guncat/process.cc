#include "guncat.ih"

void Guncat::process(istream &in)
{
    string line;

    g_lineNr = 0;
    (this->*d_getPassphrase)(in);       // called once, then resets to nop()

    d_decryptor.getPassphrase();

    while (nextline(in, line))
    {
        if (line.find("-----BEGIN PGP MESSAGE-----") == 0)
            d_decryptor.handleGPG(in, line);
        else
            cout << line << endl;
    }
}





