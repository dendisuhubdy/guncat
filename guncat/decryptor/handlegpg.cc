#include "decryptor.ih"

void Decryptor::handleGPG(istream &in, string const &pgpHeader)
{
    PwdInfo pwdInfo{ &in, in.tellg(), g_lineNr, 1 };

    while (true)
    {
        GPipe pipe;
        Process gpg(Process::CIN | Process::CERR, 
                        d_gpg + passphraseFd(pipe) + d_gpgOptions);

        if (d_timeLimit != 0)
            gpg.setTimeLimit(d_timeLimit);

        switch (insertPGPsection(gpg, pgpHeader, in))
        {
            case 2:             // incorrect passphrase
                if (retryPassphrase(pwdInfo))
                    continue;
            [[fallthrough]];
            
            default:            // some error...
                gpgError(gpg);
            [[fallthrough]];

            case 0:             // no errors
                d_firstSection = false;
            return;             // this section is now completed
        }
    }
}


