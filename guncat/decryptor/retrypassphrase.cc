#include "decryptor.ih"

bool Decryptor::retryPassphrase(PwdInfo &pwdInfo)
{
    if (not d_firstSection)
        return false;

    if (d_optionP)
        throw Exception{} << 
                "Incorrect passphrase on 1st line of input";

    if (pwdInfo.retry == 3)
        throw Exception{} << 
            "Quitting after three passphrase attempts\n";

    cerr << "Incorrect passphrase. Try again...\n";
    d_passphrase.clear();
    getPassphrase();

    if (not pwdInfo.in->seekg(pwdInfo.offset))
        throw Exception{} << "Cannot reset " << g_filename <<
                            " to the PGP section at line " << 
                            pwdInfo.lineNr;
    ++pwdInfo.retry;

    return true;
}
