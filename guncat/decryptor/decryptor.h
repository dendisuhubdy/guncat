#ifndef INCLUDED_DECRYPTOR_
#define INCLUDED_DECRYPTOR_

#include <string>
#include <fstream>

struct termios;

namespace FBB
{
    class Arg;
    class Process;
    class Pipe;
}

class Decryptor
{
    struct PwdInfo
    {
        std::istream *in;
        off_t        offset;
        size_t       lineNr;
        size_t       retry;
    };

    FBB::Arg &d_arg;
    size_t d_timeLimit = 0;
    bool d_firstSection = true;     // pwd only requested 3x at the 1st section
    bool d_optionP = false;
    bool d_errorsOK;

    std::string d_gpg;
    std::string d_gpgOptions;
    std::string d_passphrase;
    std::ofstream d_fmsg;
    std::ostream *d_msg = 0;

    public:
        Decryptor();
        ~Decryptor();
                                                // set by Guncat on -p
        void setPassphrase(std::string &passphrase);

        void getPassphrase();
                                                // line must contain the
                                                // BEGIN PGP MESSAGE header 
        void handleGPG(std::istream &in, std::string const &pgpHeader);

        void showGPG() const;                   // throws 0, called from 
                                                // Guncat()

    private:
        int echo(struct termios *ttySaved, bool reset = false) const;

        void setVerbosity();
        void addGPGoptions();                   // add --gpg-option values

        std::string passphraseFd(FBB::Pipe &pipe) const;

        int insertPGPsection(FBB::Process &gpg, std::string const &pgpHeader, 
                                std::istream &in);

        bool retryPassphrase(PwdInfo &pwdInfo);
        void gpgError(FBB::Process &gpg);
};

#endif


