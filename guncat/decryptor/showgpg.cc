#include "decryptor.ih"

void Decryptor::showGPG() const
{
    cout << d_gpg << " --pinentry-mode loopback --passphrase-fd <fd>" <<
            d_gpgOptions << '\n';
    throw 0;
}
