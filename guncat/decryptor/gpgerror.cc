#include "decryptor.ih"

void Decryptor::gpgError(Process &gpg) 
{
    ostringstream out;
    out << gpg.childErrStream().rdbuf();

    if (d_msg)
        *d_msg  << out.str() << endl;
 
    if (not d_errorsOK)
        throw Exception{} << fileInfo() << ":\n" <<
                    out.str() << "\n"
                    "Terminating " << d_arg.basename();
}
