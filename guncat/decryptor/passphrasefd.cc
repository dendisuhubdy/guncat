#include "decryptor.ih"

string Decryptor::passphraseFd(Pipe &pipe) const
{
    string ret;

    if (not d_passphrase.empty())
    {
        ret = " --pinentry-mode loopback --passphrase-fd " + 
                to_string(pipe.readFd());

        OFdStream pwd(pipe.writeFd());
        pwd << d_passphrase << endl;
    }

    return ret;
}
