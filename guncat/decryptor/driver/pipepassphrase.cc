#include "main.ih"

string pipePassphrase(Pipe &pipe)
{
    string ret{ 
        " --batch --pinentry-mode loopback --passphrase-fd " + 
            to_string(pipe.readFd()) };

    OFdStream pwd(pipe.writeFd());
    pwd << d_passphrase << endl;

    return ret;
}
