#include "main.ih"

namespace 
{
    void collect(ostream *outStream, streambuf *rdbuf)
    {
        *outStream << rdbuf << flush;
    }
}

string d_passphrase;


int main(int argc, char **argv)
try
{
    if (argc == 1)
        throw Exception{} << "1st argument must be an encrypted file";


    ifstream encrypted{ argv[1] };
    ofstream out{ argv[1] + ".dec"s };

    size_t count = 0;

    string line;
    while (getline(encrypted, line))
    {
        if (line.find("-----BEGIN PGP MESSAGE-----") != 0)
        {
            out << line << '\n';
            continue;
        }

        if (d_passphrase.empty())
        {
            getPassphrase();
        //    cout << d_passphrase << '\n';
        }


        Pipe pipe;
        string passPhraseFdOption = pipePassphrase(pipe);

     cout << ++count << ' ' << passPhraseFdOption << ", read fd = " << pipe.readFd() << '\n';
//     IFdStream in{ pipe.readFd() };
//     cout << in.rdbuf() << '\n';

        string pgpCommand{
            "/usr/bin/gpg --no-tty --no-auto-key-locate " +
            passPhraseFdOption + 
    //        " --verbose "
            " --decrypt " };
    
    //    cout << "PGP command: " << pgpCommand << '\n';

        Process gpg{ Process::ALL, pgpCommand };
        gpg.setTimeLimit(5);
    
        gpg.start();
        thread outThread(collect, &out, gpg.childOutStream().rdbuf());

        gpg << line << endl;    
        do
        {
            getline(encrypted, line);
            gpg << line << endl;
        }
        while (line.find("-----END PGP MESSAGE-----") != 0);

        gpg.close();
        outThread.join();

        cout << "waiting..." << endl;
        int ret = gpg.waitForChild();
        cout << "done: " << ret << endl;
    
        if (ret != 0)
        {
            cerr << gpg.childErrStream().rdbuf() << '\n';
            return ret;
        }
        
        close(pipe.readFd());
        close(pipe.writeFd());
    }
}
catch (exception const &exc)
{
    cerr << exc.what() << '\n';
    return 1;
}
catch (...)
{
    cerr << "unaccounted exception\n";
    return 1;
}
