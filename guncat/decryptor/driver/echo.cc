#include "main.ih"

int echo(struct termios *ttySaved, bool reset)
{
    int fd = open("/dev/tty", O_RDONLY);
    
    if (fd == -1)
        throw Exception{} << "can't open /dev/tty: " << errnodescr;

    struct termios tty; 

    tcgetattr(fd, &tty);

    if (reset)
        tty = *ttySaved;
    else
    {
        *ttySaved = tty;
        tty.c_lflag &= ~ECHO;
    }

    tcsetattr(fd, TCSANOW, &tty);

    return fd;
}
