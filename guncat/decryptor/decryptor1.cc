#include "decryptor.ih"

Decryptor::Decryptor()
:
    d_arg(Arg::instance()),
    d_errorsOK(d_arg.option(0, "errors-OK")),
    d_gpgOptions(" --no-auto-key-locate --decrypt --batch")
{
    if (not d_arg.option(&d_gpg, "gpg"))      // override default gpg location
        d_gpg = "/usr/bin/gpg";

    string value;
    if (d_arg.option(&value, 'm'))          // get the name of the msg. file
    {
        if (value == "-")
            d_msg = &cerr;                      // write to cerr
        else
        {
            Exception::open(d_fmsg, value);     // or open a file
            d_msg = &d_fmsg;
        }
    }

    if (not d_arg.option('t'))              // by default --no-tty is used
        d_gpgOptions += " --no-tty";

    value.clear();
    if (d_arg.option(&value, 'T'))
        d_timeLimit = stoul(value);         // set the gpg time-limit

    setVerbosity();                         // set GPG's verbosity

    addGPGoptions();                        // add gpg-option values

    if (d_arg.option('l')) 
        wmsg << "Option --locate-keys is disused\n";

    if (d_arg.option(0, "gpg-no-batch"))
        wmsg << "Option --gpg-no-batch is disused\n";
}




