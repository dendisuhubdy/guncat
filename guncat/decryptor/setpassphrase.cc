#include "decryptor.ih"

void Decryptor::setPassphrase(std::string &passphrase)
{
    d_passphrase = std::move(passphrase);
    d_optionP = not d_passphrase.empty();
}
        
