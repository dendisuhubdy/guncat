#include "decryptor.ih"

int Decryptor::insertPGPsection(Process &gpg, string const &pgpHeader, 
                                istream &in)
{
    gpg.start();

    if (d_msg)
        *d_msg << fileInfo() << ": PGP HEADER" << endl;

    gpg << pgpHeader << '\n';

    string line;

    do
    {
        nextline(in, line);
        gpg << line << '\n';
    }
    while (line.find("-----END PGP MESSAGE-----") != 0);

    gpg.close();
    return gpg.waitForChild();
}


